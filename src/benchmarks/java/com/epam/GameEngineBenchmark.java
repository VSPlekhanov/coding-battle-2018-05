package com.epam;

import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@Fork(1)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@State(Scope.Benchmark)
public class GameEngineBenchmark {

    @Param({"10", "100", "1000"})
    private int numberIterations;

    private boolean[][] field;
    private GameEngine engine;

    @Setup
    public void setup() {
        field = new boolean[1000][1000];
        engine = new GameEngineImpl();
        Random random = new Random();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                field[i][j] = random.nextBoolean();
            }
        }
    }

    @Benchmark
    public boolean[][] compute() {
        return engine.compute(field, numberIterations);
    }
}
