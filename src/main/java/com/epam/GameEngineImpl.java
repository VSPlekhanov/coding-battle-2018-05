package com.epam;

public class GameEngineImpl implements GameEngine {
    static boolean[] table;
    static {
        table = new boolean[512];
        for (int i = 0; i < table.length; i++) {
            int count = Integer.bitCount(i & 0b111101111);
            table[i] = ((Integer.bitCount(i & 0b000010000) == 1) && (count  == 2)) || (count == 3);
        }
    }
    private boolean[][][] initialState1;
//    private boolean[] old;
    int k;
//    int lengthI;
//    int lengthJ;

    public boolean[][] compute(boolean[][] initialState, int numberIterations) {
        initialState1 = new boolean[numberIterations + 1][initialState.length + 2][initialState[0].length + 2];
        initialState1[0] = copy(initialState);
        for (k = 0; k < numberIterations; k++) {
            for (int i = 1; i < initialState.length + 1; i++) {
                int count = (initialState1[k][i-1][0] ? 32 : 0) + (initialState1[k][i-1][1] ?  4 : 0)
                        + (initialState1[k][i][0] ? 16 : 0) + (initialState1[k][i][1] ?  2 : 0)
                        + (initialState1[k][i+1][0] ?  8 : 0) + (initialState1[k][i+1][1] ?  1 : 0);

                for (int j = 1; j < initialState[0].length + 1; j++) {
                    count = ((count % 64) * 8)
                            + (initialState1[k][i-1][j+1] ? 4 : 0)
                            + (initialState1[k][i][j+1] ? 2 : 0)
                            + (initialState1[k][i+1][j+1] ? 1 : 0);
                    initialState1[k + 1][i][j] = table[count];
                }
            }
        }
        return ret(initialState1[k]);
    }

    private boolean[][] copy(boolean[][] old){
        boolean[][] curr = new boolean[old.length + 2][ old[0].length + 2];
        for (int i = 1; i < old.length + 1; i++) {
            for (int j = 1; j < old[0].length + 1; j++) {
                curr[i][j] = old[i - 1][j - 1];
            }
        }
        for (int i = 0; i < old.length; i++) {
            curr[0][i] = curr[old.length][i];
            curr[old.length + 1][i] = curr[1][i];
        }
        for (int i = 0; i < old[0].length; i++) {
            curr[i][0] = curr[i][old[0].length];
            curr[i][old[0].length + 1] = curr[i][1];
        }
        curr[0][0] = curr[old.length][old[0].length];
        curr[old.length + 1][0] = curr[1][old[0].length];
        curr[0][old.length + 1] = curr[old.length][1];
        curr[old.length + 1][old[0].length + 1] = curr[1][1];

        return curr;
    }

    private boolean[][] ret(boolean[][] old){
        boolean[][] curr = new boolean[old.length - 2][ old[0].length - 2];
        for (int i = 1; i < old.length - 1; i++) {
            for (int j = 1; j < old[0].length - 1; j++) {
                curr[i - 1][j - 1] = old[i][j];
            }
        }

        return curr;
    }


//    @Override
//    public boolean[][] compute(boolean[][] initialState, int numberIterations) {
//        initialState1 = initialState;
//        lengthI = initialState.length;
//        lengthJ = initialState[0].length;
//        old = toArray(initialState);
//        for (int k = 0; k < numberIterations; k++) {
//            boolean[] curr = new boolean[old.length];
//            for (int i = 0; i < old.length; i++) {
//                curr[i] = isAlive(i);
//            }
//            old = curr;
//        }
//        return toDoubleArray(old);
//    }

//    private boolean[] toArray(boolean[][] old){
//        return null;
//    }
//
//    private boolean[][] toDoubleArray(boolean[] old){
//        return null;
//    }


//    private boolean isAlive(int i){
//        int count = count(i);
//        return false;
//    }
//
//    private boolean isAlive(int i, int j){
//        int count = count(i, j);
//        return (initialState1[k][i][j] && (count == 2)) || (count == 3);
//    }

//    private int count(int i){
//        int count = Boolean.compare(old[i], false);
//        count += Boolean.compare(initialState1[i][prevJ(j)], false);
//        count += Boolean.compare(initialState1[postI(i)][prevJ(j)], false);
//        count += Boolean.compare(initialState1[prevI(i)][j], false);
//
//        count += Boolean.compare(initialState1[postI(i)][j], false);
//        count += Boolean.compare(initialState1[prevI(i)][postJ(j)], false);
//        count += Boolean.compare(initialState1[i][postJ(j)], false);
//        count += Boolean.compare(initialState1[postI(i)][postJ(j)], false);
//
//        return  count;
//    }

//    private int count(int i, int j){
//        int count = initialState1[prevI(i)][prevJ(j)]? 1 : 0;
//        count += initialState1[i][prevJ(j)] ? 1 : 0;
//        count += initialState1[postI(i)][prevJ(j)] ? 1 : 0;
//        count += initialState1[prevI(i)][j] ? 1 : 0;
//
//        count += initialState1[postI(i)][j] ? 1 : 0;
//        count += initialState1[prevI(i)][postJ(j)] ? 1 : 0;
//        count += initialState1[i][postJ(j)] ? 1 : 0;
//        count += initialState1[postI(i)][postJ(j)] ? 1 : 0;
//
//        return  count;
//    }

//    private int count(int i, int j){
//        int count = Boolean.compare(initialState1[prevI(i)][prevJ(j)], false);
//        count += Boolean.compare(initialState1[i][prevJ(j)], false);
//        count += Boolean.compare(initialState1[postI(i)][prevJ(j)], false);
//        count += Boolean.compare(initialState1[prevI(i)][j], false);
//
//        count += Boolean.compare(initialState1[postI(i)][j], false);
//        count += Boolean.compare(initialState1[prevI(i)][postJ(j)], false);
//        count += Boolean.compare(initialState1[i][postJ(j)], false);
//        count += Boolean.compare(initialState1[postI(i)][postJ(j)], false);
//
//        return  count;
//    }

//    private int count(int i, int j){
//        int count = Boolean.compare(initialState1[k][i - 1][j - 1], false);
//        count += Boolean.compare(initialState1[k][i][j - 1], false);
//        count += Boolean.compare(initialState1[k][i + 1][j - 1], false);
//        count += Boolean.compare(initialState1[k][i - 1][j], false);
//
//        count += Boolean.compare(initialState1[k][i + 1][j], false);
//        count += Boolean.compare(initialState1[k][i - 1][j + 1], false);
//        count += Boolean.compare(initialState1[k][i][j + 1], false);
//        count += Boolean.compare(initialState1[k][i + 1][j + 1], false);
//
//        return  count;
//    }

//    private int prevI(int i){
//        return (initialState1.length + i - 1) % initialState1.length;
//    }
//
//    private int prevJ(int j){
//        return (initialState1[0].length + j - 1) % initialState1[0].length;
//    }
//
//
//    private int postI(int i){
//        return (i + 1) % initialState1.length;
//    }
//
//    private int postJ(int j){
//        return (j + 1) % initialState1[0].length;
//    }
//
//    public boolean[] twoDimArrayToOneDim(boolean[][] sourse) {
//        boolean[] result = new boolean[sourse.length * sourse[0].length];
//        for (int i = 0; i < sourse.length; i++) {
//            for (int j = 0; j < sourse[0].length; j++) {
//                result[i * sourse.length + j] = sourse[i][j];
//            }
//        }
//        return result;
//    }
//
//    public boolean[][] oneDimArrayToTwoDim(boolean[] sourse, int jSize) {
//        boolean[][] result = new boolean[sourse.length / jSize][jSize];
//        for (int i = 0; i < sourse.length; i++) {
//            result[i / jSize][i % jSize] = sourse[i];
//            System.out.println(i / jSize + " " + i % jSize + " " + sourse[i]);
//        }
//        return result;
//    }
}
