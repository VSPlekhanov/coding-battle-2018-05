package com.epam;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameEngineBasisConfigurationsTest {
    private static final boolean T = true;
    private static final boolean F = false;

    @Test
    void standardAliveSectorTest1() {
        boolean[][] initialState = {
            {false, false, false, false, false},
            {false, true,  false, false, false},
            {false, false, false, true, false},
            {false, false, true,  false, false},
            {false, false, false, false, false}
        };
        int numberIterations = 1;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, true,  false, false},
                {false, false, false, false, false},
                {false, false, false, false, false}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }

    @Test
    void standardAliveSectorTest2() {
        boolean[][] initialState = {
                {false, false, false, false, false},
                {false, true,  false, false, false},
                {false, false, false, true, false},
                {false, false, true,  false, false},
                {false, false, false, false, false}
        };
        int numberIterations = 2;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false,  false, false},
                {false, false, false, false, false},
                {false, false, false, false, false}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }

    @Test
    void standardAliveSectorTest3() {
        boolean[][] initialState = {
                {true, true, true, true, true},
                {true, true, true, true, true},
                {true, true, true, true, true},
                {true, true, true, true, true}
        };
        int numberIterations = 1;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false,  false, false},
                {false, false, false, false, false}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }

    @Test
    void standardAliveSectorTest4() {
        boolean[][] initialState = {
                {true}
               };
        int numberIterations = 1;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {false}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }

    @Test
    void standardAliveSectorTest5() {
        boolean[][] initialState = {
                {false}
        };
        int numberIterations = 1000;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {false}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }
    @Test
    void standardAliveSectorTest6() {
        boolean[][] initialState = {
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, T, F, F, F},
                {F, F, F, F, T, F, F},
                {F, F, T, T, T, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F}
        };
        int numberIterations = 1;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F},
                {F, F, T, F, T, F, F},
                {F, F, F, T, T, F, F},
                {F, F, F, T, F, F, F},
                {F, F, F, F, F, F, F}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }


    @Test
    void standardAliveSectorTest7() {
        boolean F = false;
        boolean[][] initialState = {
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, T, F, F, F},
                {F, F, F, F, T, F, F},
                {F, F, T, T, T, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F}
        };
        int numberIterations = 2;

        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] newInitialState = gameEngine.compute(initialState, numberIterations);

        boolean[][] expectedState = {
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, F, F, F, F},
                {F, F, F, F, T, F, F},
                {F, F, T, F, T, F, F},
                {F, F, F, T, T, F, F},
                {F, F, F, F, F, F, F}
        };

        for (int i = 0; i < newInitialState.length; i++) {
            for (int j = 0; j < newInitialState[0].length; j++) {
                assertEquals(expectedState[i][j], newInitialState[i][j]);
            }
        }
    }

}