package com.epam;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TwoOneTest {

    @Test
    void twoToOne() {
        boolean[][] arr = {{true, true, true}, {false, false,false}, {true, false,true}};
        GameEngine gameEngine = new GameEngineImpl();
        boolean[] array = ((GameEngineImpl) gameEngine).twoDimArrayToOneDim(arr);

        assertEquals(array[0], true);
        assertEquals(array[1], true);
        assertEquals(array[2], true);
        assertEquals(array[3], false);
        assertEquals(array[4], false);
        assertEquals(array[5], false);
        assertEquals(array[6], true);
        assertEquals(array[7], false);
        assertEquals(array[8], true);
    }

    @Test
    void oneToTwo() {
        boolean[] arr = {true, true, false, false, true, false};
        GameEngine gameEngine = new GameEngineImpl();
        boolean[][] array = ((GameEngineImpl) gameEngine).oneDimArrayToTwoDim(arr, 2);

        assertEquals(array[0][0], true);
        assertEquals(array[0][1], true);
        assertEquals(array[1][0], false);
        assertEquals(array[1][1], false);
        assertEquals(array[2][0], true);
        assertEquals(array[2][1], false);
    }
}